package application;


import javafx.scene.control.TextField;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import weAnimals.Schedule;

public class SampleController {
	
	private String enterDate;
	private int enterID;
	private String enterBirthdate;
	private String enterType;
	private int enterHeight;
	private int enterWeight;
	private Schedule test = new Schedule();
	
	public void createSchedule() throws Exception
	{
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL); //window priority
		window.setTitle("Create new schedule");
		
		window.setMinWidth(350);
		window.setMinHeight(350);
		
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(8);
		grid.setHgap(10);
		
		Label id = new Label("ID: ");
		GridPane.setConstraints(id, 0, 0);
		
		TextField idInput = new TextField("1");
		GridPane.setConstraints(idInput, 1, 0);
		
		Label date = new Label("Date: ");
		GridPane.setConstraints(date, 0, 1);
		
		TextField dateInput = new TextField("10/10/2018");
		GridPane.setConstraints(dateInput, 1, 1);
		
		Label birthdate = new Label("Birthdate: ");
		GridPane.setConstraints(birthdate, 0, 2);
		
		TextField inputBirthdate = new TextField("1");
		GridPane.setConstraints(inputBirthdate, 1, 2);
		
		Label type = new Label("Type: ");
		GridPane.setConstraints(type, 0, 3);
		
		TextField inputType = new TextField("1");
		GridPane.setConstraints(inputType, 1, 3);
		
		Label height = new Label("Height: ");
		GridPane.setConstraints(height, 0, 4);
		
		TextField inputHeight = new TextField("1");
		GridPane.setConstraints(inputHeight, 1, 4);
		
		Label weight = new Label("Weight: ");
		GridPane.setConstraints(weight, 0, 5);
		
		TextField inputWeight = new TextField("1");
		GridPane.setConstraints(inputWeight, 1, 5);
		
		Button enter = new Button("Enter");
		enter.setOnAction(e -> 
		{
			enterDate = dateInput.getText();
			enterID = Integer.parseInt(idInput.getText());
			enterBirthdate = inputBirthdate.getText();
			enterType = inputType.getText();
			enterHeight = Integer.parseInt(inputHeight.getText());
			enterWeight = Integer.parseInt(inputWeight.getText());
			try {
				test.createSchedule(enterID, enterDate,enterType, enterBirthdate, enterHeight, enterWeight);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		GridPane.setConstraints(enter, 1, 6);		
		
		
		grid.getChildren().addAll(id, date, birthdate, type, weight, height, idInput, dateInput, inputBirthdate, inputType, inputHeight, inputWeight, enter);
		
		Scene scene = new Scene(grid, 600, 300);
		window.setScene(scene);
		window.show();
	}
	public void deleteSchedule() throws Exception
	{
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL); //window priority
		window.setTitle("Delete by ID");
		
		window.setMinWidth(350);
		window.setMinHeight(350);
		
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(8);
		grid.setHgap(10);
		
		Label id = new Label("ID: ");
		GridPane.setConstraints(id, 0, 0);
		
		TextField idInput = new TextField("1");
		GridPane.setConstraints(idInput, 1, 0);
		
		Button delete = new Button("Delete");
		delete.setOnAction(e -> 
		{
			enterID = Integer.parseInt(idInput.getText());
			try {
				test.deleteSchedule(enterID);
			} catch (Exception e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		});
		
		grid.getChildren().addAll(id, idInput, delete);
		Scene scene = new Scene(grid, 400, 200);
		window.setScene(scene);
		window.show();
	}
	public void updateSchedule() throws Exception
	{
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL); //window priority
		window.setTitle("Create new schedule");
		
		window.setMinWidth(350);
		window.setMinHeight(350);
		
		GridPane grid = new GridPane();
		grid.setPadding(new Insets(10,10,10,10));
		grid.setVgap(8);
		grid.setHgap(10);
		
		Label id = new Label("ID: ");
		GridPane.setConstraints(id, 0, 0);
		
		TextField idInput = new TextField("1");
		GridPane.setConstraints(idInput, 1, 0);
		
		Label date = new Label("Date: ");
		GridPane.setConstraints(date, 0, 1);
		
		TextField dateInput = new TextField("10/10/2018");
		GridPane.setConstraints(dateInput, 1, 1);
		
		Label birthdate = new Label("Birthdate: ");
		GridPane.setConstraints(birthdate, 0, 2);
		
		TextField inputBirthdate = new TextField("1");
		GridPane.setConstraints(inputBirthdate, 1, 2);
		
		Label type = new Label("Type: ");
		GridPane.setConstraints(type, 0, 3);
		
		TextField inputType = new TextField("1");
		GridPane.setConstraints(inputType, 1, 3);
		
		Label height = new Label("Height: ");
		GridPane.setConstraints(height, 0, 4);
		
		TextField inputHeight = new TextField("1");
		GridPane.setConstraints(inputHeight, 1, 4);
		
		Label weight = new Label("Weight: ");
		GridPane.setConstraints(weight, 0, 5);
		
		TextField inputWeight = new TextField("1");
		GridPane.setConstraints(inputWeight, 1, 5);
		
		Button enter = new Button("Enter");
		enter.setOnAction(e -> 
		{
			enterDate = dateInput.getText();
			enterID = Integer.parseInt(idInput.getText());
			enterBirthdate = inputBirthdate.getText();
			enterType = inputType.getText();
			enterHeight = Integer.parseInt(inputHeight.getText());
			enterWeight = Integer.parseInt(inputWeight.getText());
			try {
				test.updateSchedule(enterID, enterDate, enterType, enterBirthdate, enterHeight, enterWeight);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		});
		GridPane.setConstraints(enter, 1, 6);		
		
		
		grid.getChildren().addAll(id, date, birthdate, type, weight, height, idInput, dateInput, inputBirthdate, inputType, inputHeight, inputWeight, enter);
		
		Scene scene = new Scene(grid, 600, 300);
		window.setScene(scene);
		window.show();
	}
	public void secretMessage()
	{
		Stage window = new Stage();
		window.initModality(Modality.APPLICATION_MODAL);
		window.setTitle("Secret message");
		window.setMinHeight(300);
		window.setMinWidth(500);
		
		Button pressButton = new Button("Press the button");
		pressButton.setOnAction(e -> window.close());
		
		Label label = new Label();
		label.setText("I had no idea how to continue, so I made this");
		
		VBox layout = new VBox(10);
		layout.getChildren().addAll(label, pressButton);
		layout.setAlignment(Pos.CENTER);
		
		Scene scene = new Scene(layout, 500, 300);
		window.setScene(scene);
		window.showAndWait();
	}
}
