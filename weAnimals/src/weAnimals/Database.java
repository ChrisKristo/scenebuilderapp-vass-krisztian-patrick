package weAnimals;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class Database
{
	public static EntityManagerFactory entityManagerFactory; // connection
	public static EntityManager entityManager; // transactions
	
	//methods for entities
	public void setUp() throws Exception
	{
		entityManagerFactory = Persistence.createEntityManagerFactory("weAnimals");
		entityManager = entityManagerFactory.createEntityManager();
	}
	
	//transactions
	public void startTrans()
	{
		entityManager.getTransaction().begin();
	}
	public void commitTrans()
	{
		entityManager.getTransaction().commit();
	}
	public void stopTrans()
	{
		entityManager.close();
	}
	
	//create methods
	public void create(Theschedule schedule)
	{
		entityManager.persist(schedule);
	}
	
	//update methods
	public void update(Theschedule schedule)
	{
		entityManager.merge(schedule);
	}
	
	//delete methods
	public void delete(Theschedule schedule, int id)
	{
		schedule = entityManager.find(Theschedule.class, id);
		entityManager.remove(schedule);
	}
	//display methods
	public void print()
	{
		List<Theschedule> result = entityManager.createNativeQuery("Select*From veterinary.animal", Theschedule.class).getResultList();
		for(Theschedule schedule : result)
			System.out.println("ID: " + schedule.getIdSchedule() + " Date: " + schedule.getDate() + " Type: " + schedule.getType() 
				+ " Birth date: " + schedule.getBirth_date() + " Height: " + schedule.getHeight() + " Weight: " + schedule.getWeight());
	}
}
