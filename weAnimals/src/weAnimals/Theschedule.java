package weAnimals;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the theschedule database table.
 * 
 */
@Entity
@NamedQuery(name="Theschedule.findAll", query="SELECT t FROM Theschedule t")
public class Theschedule implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private int idSchedule;

	@Column(name="`Birth date`")
	private String birth_date;

	private String date;

	private int height;

	private String type;

	private int weight;
	
	public Theschedule() {
	}
	
	//constructor
	public Theschedule(int id, String date, String birthDate, String type, int height, int weight)
	{
		this.idSchedule = id;
		this.date = date;
		this.birth_date = birthDate;
		this.type = type;
		this.height = height;
		this.weight = weight;
	}
	//constructor for updating date
	public Theschedule(int id, String date)
	{
		this.idSchedule = id;
		this.date = date;
	}

	public int getIdSchedule() {
		return this.idSchedule;
	}

	public void setIdSchedule(int idSchedule) {
		this.idSchedule = idSchedule;
	}

	public String getBirth_date() {
		return this.birth_date;
	}

	public void setBirth_date(String birth_date) {
		this.birth_date = birth_date;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public int getHeight() {
		return this.height;
	}

	public void setHeight(int height) {
		this.height = height;
	}

	public String getType() {
		return this.type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public int getWeight() {
		return this.weight;
	}

	public void setWeight(int weight) {
		this.weight = weight;
	}

}