package weAnimals;

import java.util.Scanner;

public class Schedule {
	//scanner
		Scanner input = new Scanner(System.in);
		
		//creating
		public void createSchedule(int id, String date, String birthdate, String type, int height, int weight) throws Exception
		{
			//connect to database
			Database dbUtil = new Database();
			dbUtil.setUp();
			
			dbUtil.startTrans();
			Theschedule schedule = new Theschedule(id, date, type, birthdate, height, weight);
			dbUtil.create(schedule);
			dbUtil.commitTrans();
			
			//print and close
			dbUtil.stopTrans();
		}
		//delete
		public void deleteSchedule(int id) throws Exception
		{
			//connect to database
			Database dbUtil = new Database();
			dbUtil.setUp();
			dbUtil.startTrans();
			dbUtil.delete(new Theschedule(), id);
			dbUtil.commitTrans();
			
			dbUtil.stopTrans();
		}
		//
		public void updateSchedule(int id, String date, String birthdate, String type, int height, int weight) throws Exception
		{
			//connect to database
			Database dbUtil = new Database();
			dbUtil.setUp();
			
			dbUtil.startTrans();
			dbUtil.update(new Theschedule(id, date, type, birthdate, height, weight));
			dbUtil.commitTrans();
			
			dbUtil.stopTrans();
		}
}
